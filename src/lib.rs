pub mod combinator;

pub use self::combinator::*;

#[macro_export]
macro_rules! parser {
    { $( pub struct $name:ident < $tok:ty, $err:ty > => $out:ty $body:block; )* } => {
        $(
            pub struct $name;

            impl ::combs::combinator::Combinator for $name {
                type Error = $err;
                type Token = $tok;
                type Output = $out;

                fn try_match<I>(&self, iter: &mut ::combs::BufferedIterator<I>)
                    -> Option<Result<::combs::combinator::Match<Self::Token, Self::Output>, Self::Error>>
                where
                    I: Iterator<Item = Result<Self::Token, Self::Error>>
                {
                    $body.try_match(iter)
                }
            }
        )*
    }
}

#[derive(Debug, Clone)]
pub struct BufferedIterator<I: Iterator> {
    iter: I,
    buffer: Vec<I::Item>,
}

impl<I: Iterator> BufferedIterator<I> {
    pub fn new(iter: I) -> Self {
        BufferedIterator { iter, buffer: vec![] }
    }

    pub fn iter(&self) -> &I {
        &self.iter
    }

    pub fn iter_mut(&mut self) -> &mut I {
        &mut self.iter
    }

    pub fn push_back(&mut self, item: I::Item) {
        self.buffer.push(item);
    }

    pub fn push_all<J: DoubleEndedIterator<Item = I::Item>>(&mut self, items: J) {
        self.buffer.extend(items.rev());
    }

    pub fn peek(&mut self) -> Option<&I::Item> {
        if self.buffer.is_empty() {
            match self.next() {
                Some(item) => {
                    self.buffer.push(item);
                    self.buffer.last()
                }
                None => None,
            }
        } else {
            self.buffer.last()
        }
    }

    pub fn is_empty(&mut self) -> bool {
        self.peek().is_none()
    }
}

impl<I: Iterator> Iterator for BufferedIterator<I> {
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.buffer.pop().or_else(|| self.iter.next())
    }
}
