use std::marker::PhantomData;

use super::BufferedIterator;

#[derive(Debug, Clone, PartialEq)]
pub struct Match<T, U> {
    pub token: Vec<T>,
    pub value: U,
}

impl<T, U> Match<T, U> {
    pub fn new(token: Vec<T>, value: U) -> Self {
        Match { token, value }
    }
}

pub trait Combinator: Sized {
    type Token;
    type Error;
    type Output;

    fn is_match<I>(&self, iter: &mut BufferedIterator<I>) -> bool
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.try_match(iter) {
            Some(Ok(_)) => true,
            _ => false,
        }
    }

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>;

    fn and<C>(self, other: C) -> And<Self, C, Self::Error>
    where
        C: Combinator<Token = Self::Token>,
    {
        And::new(self, other)
    }

    fn and_with<C>(self, other: C) -> AndWith<Self, C, Self::Error>
    where
        C: Combinator<Token = Self::Token>,
    {
        AndWith::new(self, other)
    }

    fn and_without<C>(self, other: C) -> AndWithout<Self, C, Self::Error>
    where
        C: Combinator<Token = Self::Token>,
    {
        AndWithout::new(self, other)
    }

    fn or<C>(self, other: C) -> Or<Self, C, Self::Error>
    where
        C: Combinator<Output = Self::Output, Token = Self::Token>,
    {
        Or::new(self, other)
    }

    fn zero_plus(self) -> ZeroPlus<Self, Self::Error> {
        ZeroPlus::new(self)
    }

    fn one_plus(self) -> OnePlus<Self, Self::Error> {
        OnePlus::new(self)
    }

    fn maybe(self) -> Maybe<Self, Self::Error> {
        Maybe::new(self)
    }

    fn map<F, U>(self, func: F) -> Map<Self, F, Self::Error>
    where
        F: Fn(&Match<Self::Token, Self::Output>) -> U,
    {
        Map::new(self, func)
    }

    fn map_match<F, U>(self, func: F) -> MapMatch<Self, F, Self::Error>
    where
        F: Fn(Match<Self::Token, Self::Output>) -> Match<Self::Token, U>,
    {
        MapMatch::new(self, func)
    }

    fn flat_map_match<F, U>(self, func: F) -> FlatMapMatch<Self, F, Self::Error>
    where
        F: Fn(Match<Self::Token, Self::Output>) -> Option<Result<Match<Self::Token, U>, Self::Error>>,
    {
        FlatMapMatch::new(self, func)
    }

    fn map_once<F, U>(self, func: F) -> MapOnce<Self, F, Self::Error>
    where
        F: Fn(Self::Output) -> U,
    {
        MapOnce::new(self, func)
    }

    fn flat_map<F, U>(self, func: F) -> FlatMap<Self, F, Self::Error>
    where
        F: Fn(&Match<Self::Token, Self::Output>) -> Option<Result<U, Self::Error>>,
    {
        FlatMap::new(self, func)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Count<T, E> {
    count: usize,
    _phantom: PhantomData<(T, E)>,
}

impl<T, E> Count<T, E> {
    pub fn new(count: usize) -> Self {
        Count {
            count,
            _phantom: PhantomData,
        }
    }
}

impl<T, E> Combinator for Count<T, E> {
    type Error = E;
    type Output = ();
    type Token = T;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        let mut count = 0;
        let mut vec = Vec::with_capacity(self.count);

        loop {
            if count == self.count {
                break;
            }

            match iter.next() {
                Some(Ok(tok)) => vec.push(tok),
                Some(Err(err)) => return Some(Err(err)),
                None => break,
            }
            count += 1;
        }

        if count == self.count {
            Some(Ok(Match::new(vec, ())))
        } else {
            iter.push_all(vec.into_iter().map(|tok| Ok(tok)));
            None
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct True<T, E> {
    _phantom: PhantomData<(T, E)>,
}

impl<T, E> True<T, E> {
    pub fn new() -> Self {
        True { _phantom: PhantomData }
    }
}

impl<T, E> Combinator for True<T, E> {
    type Error = E;
    type Output = ();
    type Token = T;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        let mut vec = vec![];

        for tok in iter {
            match tok {
                Ok(tok) => vec.push(tok),
                Err(err) => return Some(Err(err)),
            }
        }

        Some(Ok(Match::new(vec, ())))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct False<T, E> {
    _phantom: PhantomData<(T, E)>,
}

impl<T, E> False<T, E> {
    pub fn new() -> Self {
        False { _phantom: PhantomData }
    }
}

impl<T, E> Combinator for False<T, E> {
    type Error = E;
    type Output = ();
    type Token = T;

    fn try_match<I>(
        &self,
        _iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        None
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Predicate<T, F, E> {
    pred: F,
    _phantom: PhantomData<(T, E)>,
}

impl<T, F: Fn(&T) -> bool, E> Predicate<T, F, E> {
    pub fn new(pred: F) -> Self {
        Predicate {
            pred,
            _phantom: PhantomData,
        }
    }
}

impl<T, F: Fn(&T) -> bool, E> Combinator for Predicate<T, F, E> {
    type Error = E;
    type Output = ();
    type Token = T;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match iter.next() {
            Some(Ok(item)) => {
                if (self.pred)(&item) {
                    Some(Ok(Match::new(vec![item], ())))
                } else {
                    iter.push_back(Ok(item));
                    None
                }
            }
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Literal<T, E> {
    literal: T,
    _phantom: PhantomData<E>,
}

impl<T: PartialEq, E> Literal<T, E> {
    pub fn new(literal: T) -> Self {
        Literal {
            literal,
            _phantom: PhantomData,
        }
    }
}

impl<T: PartialEq, E> Combinator for Literal<T, E> {
    type Error = E;
    type Output = ();
    type Token = T;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match iter.next() {
            Some(Ok(item)) => {
                if self.literal == item {
                    Some(Ok(Match::new(vec![item], ())))
                } else {
                    iter.push_back(Ok(item));
                    None
                }
            }
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct And<T, U, E> {
    a: T,
    b: U,
    _phantom: PhantomData<E>,
}

impl<T, U, E> And<T, U, E>
where
    T: Combinator,
    U: Combinator<Token = T::Token>,
{
    pub fn new(a: T, b: U) -> Self {
        And {
            a,
            b,
            _phantom: PhantomData,
        }
    }
}

impl<T, U, E> Combinator for And<T, U, E>
where
    T: Combinator<Error = E>,
    U: Combinator<Token = T::Token, Error = E>,
{
    type Error = E;
    type Output = (T::Output, U::Output);
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.a.try_match(iter) {
            Some(Ok(mut a)) => {
                match self.b.try_match(iter) {
                    Some(Ok(b)) => {
                        a.token.extend(b.token.into_iter());
                        Some(Ok(Match::new(a.token, (a.value, b.value))))
                    }
                    Some(Err(err)) => Some(Err(err)),
                    None => {
                        iter.push_all(a.token.into_iter().map(Ok));
                        None
                    }
                }
            }
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct AndWith<T, U, E> {
    a: T,
    b: U,
    _phantom: PhantomData<E>,
}

impl<T, U, E> AndWith<T, U, E>
where
    T: Combinator,
    U: Combinator<Token = T::Token>,
{
    pub fn new(a: T, b: U) -> Self {
        AndWith {
            a,
            b,
            _phantom: PhantomData,
        }
    }
}

impl<T, U, E> Combinator for AndWith<T, U, E>
where
    T: Combinator<Error = E>,
    U: Combinator<Token = T::Token, Error = E>,
{
    type Error = E;
    type Output = U::Output;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.a.try_match(iter) {
            Some(Ok(mut a)) => {
                match self.b.try_match(iter) {
                    Some(Ok(b)) => {
                        a.token.extend(b.token.into_iter());
                        Some(Ok(Match::new(a.token, b.value)))
                    }
                    Some(Err(err)) => Some(Err(err)),
                    None => {
                        iter.push_all(a.token.into_iter().map(Ok));
                        None
                    }
                }
            }
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct AndWithout<T, U, E> {
    a: T,
    b: U,
    _phantom: PhantomData<E>,
}

impl<T, U, E> AndWithout<T, U, E>
where
    T: Combinator,
    U: Combinator<Token = T::Token>,
{
    pub fn new(a: T, b: U) -> Self {
        AndWithout {
            a,
            b,
            _phantom: PhantomData,
        }
    }
}

impl<T, U, E> Combinator for AndWithout<T, U, E>
where
    T: Combinator<Error = E>,
    U: Combinator<Token = T::Token, Error = E>,
{
    type Error = E;
    type Output = T::Output;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.a.try_match(iter) {
            Some(Ok(mut a)) => {
                match self.b.try_match(iter) {
                    Some(Ok(b)) => {
                        a.token.extend(b.token.into_iter());
                        Some(Ok(Match::new(a.token, a.value)))
                    }
                    Some(Err(err)) => Some(Err(err)),
                    None => {
                        iter.push_all(a.token.into_iter().map(Ok));
                        None
                    }
                }
            }
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Or<T, U, E> {
    a: T,
    b: U,
    _phantom: PhantomData<E>,
}

impl<T, U, E> Or<T, U, E>
where
    T: Combinator,
    U: Combinator<Output = T::Output, Token = T::Token>,
{
    pub fn new(a: T, b: U) -> Self {
        Or {
            a,
            b,
            _phantom: PhantomData,
        }
    }
}

impl<T, U, E> Combinator for Or<T, U, E>
where
    T: Combinator<Error = E>,
    U: Combinator<Output = T::Output, Token = T::Token, Error = E>,
{
    type Error = E;
    type Output = T::Output;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.a.try_match(iter) {
            Some(Ok(a)) => Some(Ok(a)),
            Some(Err(a)) => Some(Err(a)),
            None => self.b.try_match(iter),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct ZeroPlus<T, E> {
    inner: T,
    _phantom: PhantomData<E>,
}

impl<T, E> ZeroPlus<T, E>
where
    T: Combinator<Error = E>,
{
    pub fn new(inner: T) -> Self {
        ZeroPlus {
            inner,
            _phantom: PhantomData,
        }
    }
}

impl<T, E> Combinator for ZeroPlus<T, E>
where
    T: Combinator<Error = E>,
{
    type Error = E;
    type Output = Vec<T::Output>;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        let mut output = vec![];
        let mut tokens = vec![];

        loop {
            match self.inner.try_match(iter) {
                Some(Ok(m)) => {
                    tokens.extend(m.token.into_iter());
                    output.push(m.value);
                }
                Some(Err(err)) => return Some(Err(err)),
                None => break,
            }
        }

        Some(Ok(Match::new(tokens, output)))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct OnePlus<T, E> {
    inner: T,
    _phantom: PhantomData<E>,
}

impl<T, E> OnePlus<T, E>
where
    T: Combinator<Error = E>,
{
    pub fn new(inner: T) -> Self {
        OnePlus {
            inner,
            _phantom: PhantomData,
        }
    }
}

impl<T, E> Combinator for OnePlus<T, E>
where
    T: Combinator<Error = E>,
{
    type Error = E;
    type Output = Vec<T::Output>;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        let mut output = vec![];
        let mut tokens = vec![];

        loop {
            match self.inner.try_match(iter) {
                Some(Ok(m)) => {
                    tokens.extend(m.token.into_iter());
                    output.push(m.value);
                }
                Some(Err(err)) => return Some(Err(err)),
                None => break,
            }
        }

        if output.is_empty() {
            iter.push_all(tokens.into_iter().map(Ok));
            None
        } else {
            Some(Ok(Match::new(tokens, output)))
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Maybe<T, E> {
    inner: T,
    _phantom: PhantomData<E>,
}

impl<T, E> Maybe<T, E>
where
    T: Combinator<Error = E>,
{
    pub fn new(inner: T) -> Self {
        Maybe {
            inner,
            _phantom: PhantomData,
        }
    }
}

impl<T, E> Combinator for Maybe<T, E>
where
    T: Combinator<Error = E>,
{
    type Error = E;
    type Output = Option<T::Output>;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.inner.try_match(iter) {
            Some(Ok(m)) => Some(Ok(Match::new(m.token, Some(m.value)))),
            Some(Err(err)) => Some(Err(err)),
            None => Some(Ok(Match::new(vec![], None))),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Map<T, F, E> {
    inner: T,
    func: F,
    _phantom: PhantomData<E>,
}

impl<T, U, F, E> Map<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(&Match<T::Token, T::Output>) -> U,
{
    pub fn new(inner: T, func: F) -> Self {
        Map {
            inner,
            func,
            _phantom: PhantomData,
        }
    }
}

impl<T, U, F, E> Combinator for Map<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(&Match<T::Token, T::Output>) -> U,
{
    type Error = E;
    type Output = U;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.inner.try_match(iter) {
            Some(Ok(m)) => {
                let value = (self.func)(&m);
                Some(Ok(Match::new(m.token, value)))
            }
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct MapMatch<T, F, E> {
    inner: T,
    func: F,
    _phantom: PhantomData<E>,
}

impl<T, U, F, E> MapMatch<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(Match<T::Token, T::Output>) -> Match<T::Token, U>,
{
    pub fn new(inner: T, func: F) -> Self {
        MapMatch {
            inner,
            func,
            _phantom: PhantomData,
        }
    }
}

impl<T, U, F, E> Combinator for MapMatch<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(Match<T::Token, T::Output>) -> Match<T::Token, U>,
{
    type Error = E;
    type Output = U;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.inner.try_match(iter) {
            Some(Ok(m)) => Some(Ok((self.func)(m))),
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct FlatMapMatch<T, F, E> {
    inner: T,
    func: F,
    _phantom: PhantomData<E>,
}

impl<T, U, F, E> FlatMapMatch<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(Match<T::Token, T::Output>) -> Option<Result<Match<T::Token, U>, E>>,
{
    pub fn new(inner: T, func: F) -> Self {
        FlatMapMatch {
            inner,
            func,
            _phantom: PhantomData,
        }
    }
}

impl<T, U, F, E> Combinator for FlatMapMatch<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(Match<T::Token, T::Output>) -> Option<Result<Match<T::Token, U>, E>>,
{
    type Error = E;
    type Output = U;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.inner.try_match(iter) {
            Some(Ok(m)) => (self.func)(m),
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct MapOnce<T, F, E> {
    inner: T,
    func: F,
    _phantom: PhantomData<E>,
}

impl<T, U, F, E> MapOnce<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(T::Output) -> U,
{
    pub fn new(inner: T, func: F) -> Self {
        MapOnce {
            inner,
            func,
            _phantom: PhantomData,
        }
    }
}

impl<T, U, F, E> Combinator for MapOnce<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(T::Output) -> U,
{
    type Error = E;
    type Output = U;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.inner.try_match(iter) {
            Some(Ok(m)) => {
                let value = (self.func)(m.value);
                Some(Ok(Match::new(m.token, value)))
            }
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct FlatMap<T, F, E> {
    inner: T,
    func: F,
    _phantom: PhantomData<E>,
}

impl<T, U, F, E> FlatMap<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(&Match<T::Token, T::Output>) -> Option<Result<U, E>>,
{
    pub fn new(inner: T, func: F) -> Self {
        FlatMap {
            inner,
            func,
            _phantom: PhantomData,
        }
    }
}

impl<T, U, F, E> Combinator for FlatMap<T, F, E>
where
    T: Combinator<Error = E>,
    F: Fn(&Match<T::Token, T::Output>) -> Option<Result<U, E>>,
{
    type Error = E;
    type Output = U;
    type Token = T::Token;

    fn try_match<I>(
        &self,
        iter: &mut BufferedIterator<I>,
    ) -> Option<Result<Match<Self::Token, Self::Output>, Self::Error>>
    where
        I: Iterator<Item = Result<Self::Token, Self::Error>>,
    {
        match self.inner.try_match(iter) {
            Some(Ok(m)) => {
                let value = (self.func)(&m);
                match value {
                    Some(Ok(value)) => Some(Ok(Match::new(m.token, value))),
                    Some(Err(err)) => Some(Err(err)),
                    None => {
                        iter.push_all(m.token.into_iter().map(Ok));
                        None
                    }
                }
            }
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::BufferedIterator;
    use super::*;

    #[test]
    fn sanity() {
        let mut buf = BufferedIterator::new("aaa".chars().map(Ok::<_, ()>));
        assert_eq!(
            Literal::new('a').try_match(&mut buf),
            Some(Ok(Match::new(vec!['a'], ()))),
        );
        assert_eq!(buf.next(), Some(Ok('a')));
        assert_eq!(buf.next(), Some(Ok('a')));
        assert_eq!(buf.next(), None);

        let mut buf = BufferedIterator::new("bar".chars().map(Ok::<_, ()>));
        assert_eq!(Literal::new('a').try_match(&mut buf), None,);
        assert_eq!(buf.next(), Some(Ok('b')));

        let mut buf = BufferedIterator::new("aaa".chars().map(Ok::<_, ()>));
        assert_eq!(
            True::new().try_match(&mut buf),
            Some(Ok(Match::new(vec!['a', 'a', 'a'], ()))),
        );
        assert_eq!(buf.next(), None);

        let mut buf = BufferedIterator::new("aaa".chars().map(Ok::<_, ()>));
        assert_eq!(
            Count::new(2).try_match(&mut buf),
            Some(Ok(Match::new(vec!['a', 'a'], ()))),
        );
        assert_eq!(buf.next(), Some(Ok('a')));
        assert_eq!(buf.next(), None);

        let mut buf = BufferedIterator::new("aaa".chars().map(Ok::<_, ()>));
        assert_eq!(Count::new(1).try_match(&mut buf), Some(Ok(Match::new(vec!['a'], ()))),);
        assert_eq!(buf.next(), Some(Ok('a')));
        assert_eq!(buf.next(), Some(Ok('a')));
        assert_eq!(buf.next(), None);
    }

    #[test]
    fn combinators() {
        let mut buf = BufferedIterator::new("foobar".chars().map(Ok::<_, ()>));
        assert_eq!(
            Literal::new('f')
                .and(Count::new(2))
                .and(Literal::new('x').or(Literal::new('b')))
                .and(Literal::new('a'))
                .and(Predicate::new(|c: &char| *c == 'r'))
                .map(|m| {
                    m.token.iter().fold(String::new(), |mut acc, c| {
                        acc.push(*c);
                        acc
                    })
                })
                .try_match(&mut buf),
            Some(Ok(Match::new("foobar".chars().collect(), "foobar".to_string())))
        );
        assert_eq!(buf.next(), None);
    }

    #[test]
    fn quantifiers() {
        let mut buf = BufferedIterator::new("foobar".chars().map(Ok::<_, ()>));
        assert_eq!(
            Count::new(1).zero_plus().try_match(&mut buf),
            Some(Ok(Match::new("foobar".chars().collect(), vec![(); 6]))),
        );
        assert_eq!(buf.next(), None);

        let mut buf = BufferedIterator::new("foobar".chars().map(Ok::<_, ()>));
        assert_eq!(
            Count::new(1).one_plus().try_match(&mut buf),
            Some(Ok(Match::new("foobar".chars().collect(), vec![(); 6]))),
        );
        assert_eq!(buf.next(), None);

        let mut buf = BufferedIterator::new("foobar".chars().map(Ok::<_, ()>));
        assert_eq!(
            Count::new(1).maybe().try_match(&mut buf),
            Some(Ok(Match::new("f".chars().collect(), Some(())))),
        );
        assert_eq!(buf.next(), Some(Ok('o')));
    }
}
